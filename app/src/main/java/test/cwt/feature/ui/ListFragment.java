package test.cwt.feature.ui;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;
import test.cwt.R;
import test.cwt.common.model.Employee;
import test.cwt.common.ListAdapter;
import test.cwt.feature.presentation.ListPresenter;
import test.cwt.feature.presentation.ListView;

public class ListFragment extends MvpAppCompatFragment implements ListView {
    private RecyclerView recyclerView;

    @InjectPresenter
    ListPresenter listPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = view.findViewById(R.id.recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        return view;
    }

    @Override
    public void updateView(List<Employee> employees) {
        ListAdapter listAdapter = new ListAdapter(employees);
        recyclerView.setAdapter(listAdapter);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showToast(String message) {
        Toast toast = Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }
}
