package test.cwt.feature.presentation;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;
import test.cwt.common.model.Employee;

public interface ListView extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void updateView(List<Employee> dateList);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showToast(String message);
}
