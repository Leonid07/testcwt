package test.cwt.feature.presentation;

import androidx.annotation.NonNull;

import java.util.Collections;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.cwt.R;
import test.cwt.common.model.ListModel;
import test.cwt.common.NetworkService;

@InjectViewState
public class ListPresenter extends MvpPresenter<ListView> {

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadList();
    }

    public void loadList() {

        NetworkService.getInstance()
                .getJSONApi()
                .company()
                .enqueue(new Callback<ListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ListModel> call, @NonNull Response<ListModel> response) {
                        ListModel list = response.body();

                        if (list != null) {
                            Collections.sort(list.getCompany().getEmployees(), (item1, item2) -> {
                                if (item1.getName() == null && item2.getName() == null)
                                    return 0;
                                if (item1.getName() == null)
                                    return 1;
                                if (item2.getName() == null)
                                    return -1;
                                return item1.getName().compareTo(item2.getName());
                            });
                            getViewState().updateView(list.getCompany().getEmployees());
                        } else {
                            getViewState().showToast(String.valueOf(R.string.noData));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ListModel> call, @NonNull Throwable t) {
                        getViewState().showToast(String.valueOf(R.string.testInternet));
                        t.printStackTrace();
                    }
                });
    }
}
