package test.cwt.common;

import retrofit2.Call;
import retrofit2.http.GET;
import test.cwt.common.model.ListModel;

public interface JSONPlaceHolderApi {
    @GET("/v2/5ddcd3673400005800eae483")
    Call<ListModel> company();
}