package test.cwt.common;

import android.app.Application;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class TestApplication extends Application {
    public static TestApplication Instance;
    private Cicerone<Router> cicerone;

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;
        initCicerone();
    }

    private void initCicerone() {
        cicerone = cicerone.create();
    }

    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }
}
