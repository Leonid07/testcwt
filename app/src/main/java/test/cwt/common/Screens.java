package test.cwt.common;

import androidx.fragment.app.Fragment;

import ru.terrakok.cicerone.android.support.SupportAppScreen;
import test.cwt.feature.ui.ListFragment;

public class Screens {
    public static final SupportAppScreen listScreen = new ListScreen();

    static class ListScreen extends SupportAppScreen{
        @Override
        public Fragment getFragment(){return new ListFragment();}
    }
}
