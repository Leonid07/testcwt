package test.cwt.common;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import test.cwt.R;
import test.cwt.common.model.Employee;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private List<Employee> employeeList;

    public ListAdapter(List<Employee> employees) {
        employeeList = employees;
    }

    @NonNull
    @Override
    public ListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        ListAdapter.ListViewHolder ListViewHolder = new ListAdapter.ListViewHolder(view);
        return ListViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        String name = employeeList.get(position).getName();
        String phoneNumber = employeeList.get(position).getPhoneNumber();

        if (employeeList.get(position).getSkills() == null) {
            holder.postSkills.setText(" ");
        } else {
            String skills = Arrays.toString(employeeList.get(position).getSkills().toArray());
            holder.postSkills.setText(skills);
        }

        holder.postName.setText(name);
        holder.postPhoneNumber.setText(phoneNumber);
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        TextView postName;
        TextView postPhoneNumber;
        TextView postSkills;

        public ListViewHolder(View itemView) {
            super(itemView);
            postName = itemView.findViewById(R.id.name);
            postPhoneNumber = itemView.findViewById(R.id.phoneNumber);
            postSkills = itemView.findViewById(R.id.skills);
        }
    }
}
