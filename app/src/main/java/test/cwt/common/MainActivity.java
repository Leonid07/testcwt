package test.cwt.common;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import test.cwt.R;

public class MainActivity extends AppCompatActivity {
    private Navigator navigator = new SupportAppNavigator(this, R.id.container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onResume() {
        super.onResume();
        TestApplication.Instance.getNavigatorHolder().setNavigator(navigator);
        TestApplication.Instance.getRouter().newRootScreen(Screens.listScreen);
    }

    @Override
    protected void onPause() {
        super.onPause();
        TestApplication.Instance.getNavigatorHolder().removeNavigator();
    }
}